package ru.t1.tbobkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supproted...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Argument ''" + argument + "'' not supported...");
    }

}
