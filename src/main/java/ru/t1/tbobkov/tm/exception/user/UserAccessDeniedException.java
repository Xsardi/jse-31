package ru.t1.tbobkov.tm.exception.user;

public class UserAccessDeniedException extends AbstractUserException {

    public UserAccessDeniedException() {
        super("Error! Current user doesn't have access to this object...");
    }

}
